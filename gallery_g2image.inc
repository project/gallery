<?php

/**
 * gallery.module : gallery_g2image.inc
 * Support functions for g2image by capt_kirk (from http://g2image.steffensenfamily.com)
 */

/**
 * Function gallery_g2image_add_js().
 */
function gallery_g2image_add_js() {
  // Ensure only sent once
  static $sent = FALSE;
  if (!$sent) {
    $path = drupal_get_path('module', 'gallery');
    $g2image_uri = base_path() . $path .'/g2image/';
    
    drupal_add_js(array('gallery2' => array('g2image_uri' => $g2image_uri)), 'setting');
    drupal_add_js($path .'/g2image.js');
    
    $sent = TRUE;
  }
}

/**
 * Theme function : theme_gallery_g2image_textarea_link().
 * (for adding an image link underneath textareas)
 */
function theme_gallery_g2image_textarea_link($element, $link) {
  $output = '<div class="g2image-button"><a class="g2image-link" id="g2image-link-'. $element['#id']
    .'" title="'. t('Click here to add images from Gallery2 albums') 
    .'" href="#" onclick="g2ic_open(\''. $element['#id'] .'\');">';
  $output .= t('Add Gallery2 images');
  $output .= '</a></div>';
  
  return $output;
}

/**
 * Function _gallery_g2image_page_match().
 * (determine if g2image button should be attached to the page/textarea)
 *
 * @return
 *   TRUE if can render, FALSE if not allowed.
 */     
function _gallery_g2image_page_match() {
  require_once(drupal_get_path('module', 'gallery') .'/gallery_help.inc');
  
  $page_match = FALSE;
  $only_listed_pages = variable_get('gallery_g2image_only_listed_pages', 1);
  if ($pages = variable_get('gallery_g2image_std_pages', gallery_help('admin/settings/gallery_g2image#pages'))) {
    $path = drupal_get_path_alias($_GET['q']);
    $regexp = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1'. variable_get('site_frontpage', 'node') .'\2'), preg_quote($pages, '/')) .')$/';      
    $page_match = !($only_listed_pages xor preg_match($regexp, $path));    
  }
  
  return $page_match;  
} 
