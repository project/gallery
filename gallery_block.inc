<?php

/**
 * gallery.module : gallery_block.inc
 * Block functions
 */
 
 /**
 * Implementation of hook_block().
 *
 * 0 - gallery imageblock (random, most viewed, etc)
 * 1 - gallery navigation block (recommended)
 * 2 - gallery grid block
 */
function _gallery_block($op = 'list', $delta = 0, $edit = array()) {
  $type_map = $param_map = array();
  _gallery_block_options($type_map, $param_map);
  
  list($type, $delta) = (stristr($delta, '-') !== FALSE) ? explode('-', $delta) : array('', 0);
  switch ($op) {
    case 'list':
      $blocks['navigation']['info'] = t('Gallery Navigation');
      $imageblock_num = variable_get('gallery_block_image_num', 1);
      for ($i=0; $i<$imageblock_num; $i++) {
        $id = variable_get('gallery_block_image_'. $i .'_blockid', '');
        $blocks['image-'. $i]['info'] = t('Gallery Image Block @id',
          array('@id' => ($imageblock_num > 1) ? ('['. ($id ? $id : $i+1) .']') : ''));
      }
      $gridblock_num = variable_get('gallery_block_grid_num', 1);
      for ($i=0; $i<$gridblock_num; $i++) {
        $id = variable_get('gallery_block_grid_'. $i .'_blockid', '');
        $blocks['grid-'. $i]['info'] = t('Gallery Grid Block @id',
          array('@id' => ($gridblock_num > 1) ? ('['. ($id ? $id : $i+1) .']') : ''));
      }
      return $blocks;
    case 'view':
      if (!user_access('access gallery') || !_gallery_init(TRUE) || $_GET['q'] == 'admin/settings/gallery/install') {
        return;
      }
      switch ($type) {
        case 'navigation': // Navigation Block
          global $_gallery_sidebar_;
          if ((arg(0) == 'gallery') && !empty($_gallery_sidebar_)) {
            $block['subject'] = t('Gallery Navigation');
            $block['content'] = '<div id="gsSidebar" class="gcBorder1">'. implode('', $_gallery_sidebar_) .'</div>';    
          }
          break;
        case 'image': // Image Block
          $block = _gallery_block_image_block($delta, $type_map);
          break;
        case 'grid': // Image Grid Block
          $block = _gallery_block_grid_block($delta, $type_map);
          break;
      }
      GalleryEmbed::done();
      return $block;
    case 'configure':
      require_once(drupal_get_path('module', 'gallery') .'/gallery_settings.inc');
      switch ($type) {
        case 'image': // Image Block
          return _gallery_settings_block_image($delta);
        case 'grid': // Image Grid Block
          return _gallery_settings_block_grid($delta);
      }
    case 'save':
      require_once(drupal_get_path('module', 'gallery') .'/gallery_settings.inc');
      _gallery_settings_block_save($delta, $edit);
      break;
  }
}

/**
 * Function _gallery_block_options().
 */
function _gallery_block_options(&$type_map, &$param_map) {
  $type_map = array(
    'randomImage' => t('Random image'),
    'recentImage' => t('Recent image'),
    'viewedImage' => t('Viewed image'),
    'randomAlbum' => t('Random album'),
    'recentAlbum' => t('Recent album'),
    'viewedAlbum' => t('Viewed album'),
    'dailyImage' => t('Daily image'),
    'weeklyImage' => t('Weekly image'),
    'monthlyImage' => t('Monthly image'),
    'dailyAlbum' => t('Daily album'),
    'weeklyAlbum' => t('Weekly album'),
    'monthlyAlbum' => t('Monthly album'),
  );
  $param_map = array(
    'title' => t('Title'),
    'date' => t('Date'),
    'views' => t('View Count'),
    'owner' => t('Item owner'),
    'heading' => t('Heading'),
  );
}

/**
 * Function _gallery_block_image_block().
 */
function _gallery_block_image_block($delta, $type_map) {
  $param_blocks_array = variable_get('gallery_block_image_'. $delta .'_block_block', array('randomImage'));
  $params['blocks'] = is_array($param_blocks_array) ? implode('|', $param_blocks_array) : ''; 
  $param_itemid = variable_get('gallery_block_image_'. $delta .'_item_id', '');
  if (trim($param_itemid) == '%user') {
    require_once(drupal_get_path('module', 'gallery') .'/gallery_user.inc');
    $param_itemid = gallery_user_useralbum(NULL, FALSE);
  }
  if (!empty($param_itemid)) {
    $params['itemId'] =  $param_itemid;
  }
  $param_show_array = variable_get('gallery_block_image_'. $delta .'_block_show', array());
  $params['show'] = is_array($param_show_array) ? implode('|', $param_show_array) : '';
  if (variable_get('gallery_block_image_'. $delta .'_size_method', GALLERY_IMAGEBLOCK_SIZE_METHOD_DEFAULT) == 'maxsize') {
    $params['maxSize'] = variable_get('gallery_block_image_'. $delta .'_size', GALLERY_IMAGEBLOCK_SIZE_DEFAULT);
  } 
  else {
    $params['exactSize'] = variable_get('gallery_block_image_'. $delta .'_size', GALLERY_IMAGEBLOCK_SIZE_DEFAULT);
  }
  $params['albumFrame'] = variable_get('gallery_block_image_'. $delta .'_album_frame', 'none');
  $params['itemFrame'] = variable_get('gallery_block_image_'. $delta .'_item_frame', 'none');
  $params['linkTarget'] = variable_get('gallery_block_image_'. $delta .'_link_target', '');
  $params['link'] = variable_get('gallery_block_image_'. $delta .'_link', '');

  $block = array();
  list($ret, $content, $head) = GalleryEmbed::getImageBlock($params);
  if ($ret) {
    gallery_error(t('Unable to get Gallery image block'), $ret);
    return array();
  }
  
  if ($content) {
    if (count($param_blocks_array) > 1) {
      $block['subject'] = t('Gallery');
    }
    else {
      $block['subject'] = $type_map[$params['blocks']]; 
    }
    $block['content'] = theme('gallery_block_image_block', $content);
  }
  if ($head) {
    gallery_set_head($head);
  }
  
  return $block;
}

/**
 * Theme function : theme_gallery_block_image_block().
 */
function theme_gallery_block_image_block($content) {    
  return '<div class="g2image_centered">'. $content .'</div>';
}

/**
 * Function _gallery_block_grid_block().
 */
function _gallery_block_grid_block($delta, $type_map) {
  $num_cols = variable_get('gallery_block_grid_'. $delta .'_num_cols', 2);
  $num_rows = variable_get('gallery_block_grid_'. $delta .'_num_rows', 2);
  $num_images = $num_cols * $num_rows; 
  
  $param_blocks_array = array_fill(0, $num_images, variable_get('gallery_block_grid_'. $delta .'_block_block', 'randomImage'));
  $params['blocks'] = is_array($param_blocks_array) ? implode('|', $param_blocks_array) : '';
  $param_itemid = variable_get('gallery_block_grid_'. $delta .'_item_id', '');
  if (trim($param_itemid) == '%user') {
    require_once(drupal_get_path('module', 'gallery') .'/gallery_user.inc');
    $param_itemid = gallery_user_useralbum(NULL, FALSE);
  }
  if (!empty($param_itemid)) {
    $params['itemId'] =  $param_itemid;
  }
  $param_show_array = variable_get('gallery_block_grid_'. $delta .'_block_show', array());
  $params['show'] = is_array($param_show_array) ? implode('|', $param_show_array) : '';
  if (variable_get('gallery_block_grid_'. $delta .'_size_method', GALLERY_GRID_SIZE_METHOD_DEFAULT) == 'maxsize') {
    $params['maxSize'] = variable_get('gallery_block_grid_'. $delta .'_size', GALLERY_GRID_SIZE_DEFAULT);
  } 
  else {
    $params['exactSize'] = variable_get('gallery_block_grid_'. $delta .'_size', GALLERY_GRID_SIZE_DEFAULT);
  }
  $params['albumFrame'] =  variable_get('gallery_block_grid_'. $delta .'_album_frame', 'none');
  $params['itemFrame'] =  variable_get('gallery_block_grid_'. $delta .'_item_frame', 'none');
  $params['linkTarget'] =  variable_get('gallery_block_grid_'. $delta .'_link_target', '');
  $params['link'] = variable_get('gallery_block_grid_'. $delta .'_link', '');
  
  $block = array();
  list($ret, $content, $head) = GalleryEmbed::getImageBlock($params);
  if ($ret) {
    gallery_error(t('Unable to get Gallery image block'), $ret);
    return array();
  }
  if ($content) {
    $block['subject'] = t('Gallery');       
    $images = _gallery_split_imageblock($content);
    $images = array_chunk($images, $num_cols);
    $block['content'] = theme('gallery_block_grid_block', $images);
  }
  if ($head) {
    gallery_set_head($head);
  }
  
  return $block;
}

/**
 * Theme function : theme_gallery_block_grid_block().
 */
function theme_gallery_block_grid_block($images) {
  $rows = array();
  foreach ($images as $image_row) {
    $row = array();
    foreach ($image_row as $image) {
      $row[] = array('data' => $image, 'style' => 'text-align:center;');
    }
    $rows[] = $row;
  }
  
  $html  = '<div class="gallery-grid-block"><div class="image-grid">';
  $html .= theme('table', array(), $rows);
  $html .= '</div></div>';
  
  return $html;
}
